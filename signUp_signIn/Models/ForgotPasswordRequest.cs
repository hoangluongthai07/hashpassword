﻿namespace signUp_signIn.Models
{
    public class ForgotPasswordRequest
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
}
