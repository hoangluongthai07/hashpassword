﻿    namespace signUp_signIn.Models
    {
        public class User
        {
            public int Id { get; set; }
            public string Username { get; set; }
            public string HashedPassword { get; set; }
            public string Salt { get; set; }
            public string Email { get; set; }
            public string RollNumber { get; set; }
            public string PhoneNumber { get; set; }
        }
    }
