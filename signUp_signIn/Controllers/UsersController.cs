﻿using Microsoft.AspNetCore.Mvc;
using signUp_signIn.Models;
using signUp_signIn.Services;

namespace signUp_signIn.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserServiceClass _userService;

        public UsersController(IUserServiceClass userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterRequest request)
        {
            bool result = await _userService.RegisterAsync(request);

            if (result)
            {
                return Ok("Registration successful.");
            }
            else
            {
                return BadRequest("Username already exists.");
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            bool result = await _userService.LoginAsync(request);

            if (result)
            {
                return Ok("Login successful.");
            }
            else
            {
                return BadRequest("Invalid username or password.");
            }
        }
        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordRequest request)
        {
            if (request.NewPassword != request.ConfirmNewPassword)
            {
                return BadRequest("New password and confirm new password do not match.");
            }

            bool result = await _userService.ForgotPassword(request.Username, request.Email, request.NewPassword);
            if (result)
            {
                return Ok("Password reset successful. You can login with the new password.");
            }
            else
            {
                return BadRequest("Invalid username or email.");
            }
        }
    }
}
