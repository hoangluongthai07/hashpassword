﻿using Microsoft.AspNetCore.Mvc;
using signUp_signIn.Models;
using signUp_signIn.Services;

namespace signUp_signIn.Controllers
{
    public class TodoTasksController : Controller
    {
        private readonly ITodoTaskService _todoTaskService;

        public TodoTasksController(ITodoTaskService todoTaskService)
        {
            _todoTaskService = todoTaskService;
        }

        [HttpGet("GetAllTask")]
        public async Task<IActionResult> GetAllTodoTasks(int userId)
        {
            var todoTasks = await _todoTaskService.GetAllTodoTasksAsync(userId);
            return Ok(todoTasks);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoTaskById(int id)
        {
            var todoTask = await _todoTaskService.GetTodoTaskByIdAsync(id);
            if (todoTask == null)
            {
                return NotFound();
            }

            return Ok(todoTask);
        }

        [HttpPost("CreateTask")]
        public async Task<IActionResult> CreateTodoTask(CreateTodoTaskRequest request, int userId)
        {
            var todoTask = new TodoTask
            {
                Title = request.Title,
                Description = request.Description,
                DueDate = request.DueDate,
                IsCompleted = request.IsCompleted,
                UserId = userId
            };

            var createdTodoTask = await _todoTaskService.CreateTodoTaskAsync(todoTask);
            return CreatedAtAction(nameof(GetTodoTaskById), new { id = createdTodoTask.Id }, createdTodoTask);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTodoTask(int id, UpdateTodoTaskRequest request)
        {
            var todoTask = await _todoTaskService.GetTodoTaskByIdAsync(id);
            if (todoTask == null)
            {
                return NotFound();
            }

            todoTask.Title = request.Title;
            todoTask.Description = request.Description;
            todoTask.DueDate = request.DueDate;
            todoTask.IsCompleted = request.IsCompleted;

            var updatedTodoTask = await _todoTaskService.UpdateTodoTaskAsync(todoTask);
            return Ok(updatedTodoTask);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoTask(int id)
        {
            await _todoTaskService.DeleteTodoTaskAsync(id);
            return NoContent();
        }
    }
}
