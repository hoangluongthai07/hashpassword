﻿    using signUp_signIn.Models;
    namespace signUp_signIn.Services
    {
        public interface IUserServiceClass
        {
            Task<bool> RegisterAsync(RegisterRequest request);
            Task<bool> LoginAsync(LoginRequest request);
            Task<bool> ForgotPassword(string username, string email, string newPassword);

        }
    }
