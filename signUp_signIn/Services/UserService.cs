﻿using Microsoft.EntityFrameworkCore;
using signUp_signIn.Data;
using signUp_signIn.Models;
using System.Security.Cryptography;
using System.Text;
namespace signUp_signIn.Services
{
    public class UserService : IUserServiceClass
    {
        private readonly UserDbContext _context;

        public UserService(UserDbContext context)
        {
            _context = context;
        }

        public async Task<bool> RegisterAsync(RegisterRequest request)
        {
            // Check if user already exists
            if (await _context.Users.AnyAsync(u => u.Username == request.Username))
            {
                return false;
            }

            // Generate a random salt
            string salt = GenerateSalt();

            // Hash the password using SHA-256
            string hashedPassword = HashPassword(request.Password, salt);

            // Create a new user
            User newUser = new User
            {
                Username = request.Username,
                HashedPassword = hashedPassword,
                Salt = salt,
                Email = request.Email,
                RollNumber = request.RollNumber,
                PhoneNumber = request.PhoneNumber
            };

            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> LoginAsync(LoginRequest request)
        {
            // Find the user
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Username == request.Username);

            if (user == null)
            {
                return false;
            }

            // Verify the password
            string hashedPassword = HashPassword(request.Password, user.Salt);

            if (user.HashedPassword != hashedPassword)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ForgotPassword(string username, string email, string newPassword)
        {
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username && u.Email == email);
            if (user == null)
            {
                return false;
            }

            string hashedPassword = HashPassword(newPassword, user.Salt);
            user.HashedPassword = hashedPassword;
            await _context.SaveChangesAsync();

            return true;
        }

        public string HashPassword(string password, string salt)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(password);
                byte[] hashBytes = sha256.ComputeHash(inputBytes);
                return Convert.ToHexString(hashBytes);
            }
        }
        private string GenerateSalt()
        {
            byte[] randomBytes = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomBytes);
            }

            return Convert.ToBase64String(randomBytes);
        }
    }
}
