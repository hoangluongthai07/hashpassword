﻿using Microsoft.EntityFrameworkCore;
using signUp_signIn.Data;
using signUp_signIn.Models;

namespace signUp_signIn.Services
{
    public class TodoTaskService : ITodoTaskService
    {
        private readonly UserDbContext _context;

        public TodoTaskService(UserDbContext context)
        {
            _context = context;
        }

        public async Task<List<TodoTask>> GetAllTodoTasksAsync(int userId)
        {
            return await _context.TodoTasks.Where(t => t.UserId == userId).ToListAsync();
        }

        public async Task<TodoTask> GetTodoTaskByIdAsync(int id)
        {
            return await _context.TodoTasks.Include(t => t.User).FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<TodoTask> CreateTodoTaskAsync(TodoTask todoTask)
        {
            _context.TodoTasks.Add(todoTask);
            await _context.SaveChangesAsync();
            return todoTask;
        }

        public async Task<TodoTask> UpdateTodoTaskAsync(TodoTask todoTask)
        {
            _context.TodoTasks.Update(todoTask);
            await _context.SaveChangesAsync();
            return todoTask;
        }

        public async Task DeleteTodoTaskAsync(int id)
        {
            var todoTask = await _context.TodoTasks.FindAsync(id);
            if (todoTask != null)
            {
                _context.TodoTasks.Remove(todoTask);
                await _context.SaveChangesAsync();
            }
        }
    }
}
