﻿using signUp_signIn.Models;

namespace signUp_signIn.Services
{
    public interface ITodoTaskService
    {
        Task<List<TodoTask>> GetAllTodoTasksAsync(int userId);
        Task<TodoTask> GetTodoTaskByIdAsync(int id);
        Task<TodoTask> CreateTodoTaskAsync(TodoTask todoTask);
        Task<TodoTask> UpdateTodoTaskAsync(TodoTask todoTask);
        Task DeleteTodoTaskAsync(int id);
    }
}
